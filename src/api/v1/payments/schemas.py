from pydantic import BaseModel, Field


class PaymentSchema(BaseModel):
    quantity: float
    number: int
    loan_id: str
    created: str
    modified: str
