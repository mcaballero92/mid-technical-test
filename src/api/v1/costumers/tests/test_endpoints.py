import unittest

from fastapi import status, Depends
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from db.session import get_session
from core.settings import settings
from db.models import Customer
from main import app


class TestCustomerEndpoints(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)
        self.url = "/" + settings.API_V1 + "/customers"

    def test_create_customer(self):
        # Prueba para la función create
        customer_data = {"full_name": "Nombre Completo", "email": "mail@example.com",
                         "id": "e85a4b19-f7be-4a18-5eb3-28fb503dbd46", "created": "2024-03-22T23:16:28.856804+00:00",
                         "modified": "2024-03-22T23:16:28.856804+00:00", }
        response = self.client.post(self.url+"/customer", json=customer_data)
        self.assertEqual(response.status_code, 200)  # Verifica que se haya creado el cliente
        created_customer = response.json()
        self.assertIn("id", created_customer)  # Verifica que el cliente tenga un ID asignado

    def test_get_customers(self):
        # Prueba para la función get
        response = self.client.get(self.url+"/customers")
        self.assertEqual(response.status_code, 200)  # Verifica que se haya obtenido la lista de clientes

    def test_get_customer_by_id(self, db: Session = Depends(get_session)):
        # Prueba para la función get_id
        # Primero, creamos un cliente para asegurarnos de que haya al menos uno en la base de datos
        new_customer = Customer(full_name="Full Name", email="email@example.com",
                                id="e85a4b19-f7be-4a18-5eb3-28fb503dbd46", created="2024-03-22T23:16:28.856804+00:00",
                                modified="2024-03-22T23:16:28.856804+00:00")
        db.add(new_customer)
        db.commit()

        # Luego, intentamos obtener este cliente por su ID
        response = self.client.get(self.url+f"/customer/{new_customer.id}")
        self.assertEqual(response.status_code, 200)  # Verifica que se haya obtenido el cliente

    def test_delete_customer(self, db: Session = Depends(get_session)):
        # Prueba para la función delete
        # Primero, creamos un cliente para luego eliminarlo
        new_customer = Customer(full_name="Full Name", email="email@example.com",
                                id="e85a4b19-f7be-4a18-5eb3-28fb503dbd46", created="2024-03-22T23:16:28.856804+00:00",
                                modified="2024-03-22T23:16:28.856804+00:00")
        db.add(new_customer)
        db.commit()

        # Luego, intentamos eliminar este cliente por su ID
        response = self.client.delete(self.url+f"/customer/{new_customer.id}")
        self.assertEqual(response.status_code, 200)  # Verifica que se haya eliminado el cliente

    def test_update_customer(self, db: Session = Depends(get_session)):
        # Prueba para la función update
        # Primero, creamos un cliente para luego actualizarlo
        new_customer = Customer(full_name="Full Name", email="email@example.com",
                                id="e85a4b19-f7be-4a18-5eb3-28fb503dbd46", created="2024-03-22T23:16:28.856804+00:00",
                                modified="2024-03-22T23:16:28.856804+00:00")
        db.add(new_customer)
        db.commit()

        # Datos actualizados para el cliente
        updated_data = {"full_name": "New Name", "email": "newemail@example.com",
                        "id": "e85a4b19-f7be-4a18-5eb3-28fb503dbd46", "created": "2024-03-22T23:16:28.856804+00:00",
                        "modified": "2024-03-22T23:16:28.856804+00:00"}

        # Actualizamos el cliente con los datos actualizados
        response = self.client.put(self.url+f"/customer/{new_customer.id}", json=updated_data)
        self.assertEqual(response.status_code, 200)  # Verifica que se haya actualizado el cliente

        # Limpiar los datos de prueba
        db.query(Customer).filter_by(id=new_customer.id).delete()
        db.commit()

