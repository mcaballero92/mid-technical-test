from pydantic import BaseModel


class CustomerSchema(BaseModel):
    full_name: str
    email: str
    id: str
    created: str
    modified: str
