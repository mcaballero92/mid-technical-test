from fastapi import FastAPI, APIRouter, status, Depends
from api.v1.costumers.schemas import CustomerSchema
from sqlalchemy.orm import Session
from db.session import get_session
from db.models import Customer
from fastapi.exceptions import HTTPException
from core.settings import settings
from core.utils.responses import EnvelopeResponse
from pydantic import BaseModel

router = APIRouter(prefix="/customers", tags=["Customers"])


#
# Método HTTP: POST
# Ruta: "/customer"
# Descripción: Crea un nuevo cliente en la base de datos.
# Parámetros:
# customer: Objeto JSON que representa los datos del cliente a crear. Debe seguir la estructura definida en CustomerSchema.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve los datos del cliente recién creado.
@router.post("/customer")
def create(customer: CustomerSchema, db: Session = Depends(get_session)):
    new_customer = Customer(**customer.dict())
    db.add(new_customer)
    db.commit()
    db.refresh(new_customer)
    return new_customer


# Método HTTP: GET
# Ruta: "/customers"
# Descripción: Obtiene una lista de clientes de la base de datos, opcionalmente filtrados por una cadena de búsqueda, con paginación.
# Parámetros:
# db: Sesión de base de datos obtenida mediante la función get_session.
# limit: Número máximo de clientes a devolver por página (por defecto 10).
# page: Página de resultados a devolver (por defecto 1).
# search: Cadena opcional para filtrar los clientes por su nombre completo.
# Respuesta: Devuelve una lista de clientes según los parámetros proporcionados.
@router.get("/customers")
def get(db: Session = Depends(get_session), limit: int = 10, page: int = 1, search: str = ""):
    skip = (page - 1) * limit
    all_customers = (db.query(Customer)
                     .filter(Customer.full_name.contains(search))
                     .limit(limit)
                     .offset(skip)
                     .all())
    return all_customers

# Método HTTP: GET
# Ruta: "/customer/{id}"
# Descripción: Obtiene los detalles de un cliente específico según su ID.
# Parámetros:
# id: ID del cliente que se desea obtener.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve los datos del cliente correspondiente al ID proporcionado.
@router.get("/customer/{id}")
def get_id(id: str, db: Session = Depends(get_session)):
    customer = db.query(Customer).filter(Customer.id == id).first()
    return customer


# Método HTTP: DELETE
# Ruta: "/customer/{id}"
# Descripción: Elimina un cliente de la base de datos según su ID.
# Parámetros:
# id: ID del cliente que se desea eliminar.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve un mensaje indicando que el cliente ha sido eliminado correctamente o un error 404 si el cliente no existe.
@router.delete("/customer/{id}")
def delete(id: str, db: Session = Depends(get_session)):
    found = db.query(Customer).filter(Customer.id == id).first()
    if not found:
        raise HttpException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Doesn't exist")
    else:
        db.query(Customer).filter(Customer.id == id).delete()
        db.commit()
    return {"delete data": found}


# Método HTTP: PUT
# Ruta: "/customer/{id}"
# Descripción: Actualiza los datos de un cliente existente en la base de datos.
# Parámetros:
# id: ID del cliente que se desea actualizar.
# customer: Objeto JSON que contiene los datos actualizados del cliente. Debe seguir la estructura definida en CustomerSchema.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve los datos actualizados del cliente o un error 404 si el cliente no existe.
@router.put("/customer/{id}")
def update(id: str, customer: CustomerSchema, db: Session = Depends(get_session)):
    found = db.query(Customer).filter(Customer.id == id).first()
    if not found:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Customer with ID {id} does not exist")
    db.query(Customer).filter(Customer.id == id).update(customer.dict(), synchronize_session=False)
    db.commit()
    return customer.dict()
