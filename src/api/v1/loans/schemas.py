from pydantic import BaseModel, Field


class LoanSchema(BaseModel):
    amount: float
    tax: float
    fee: float
    customer_id: str
    created: str
    modified: str

class LoanSchemaCreate(BaseModel):
    amount: float
    customer_id: str
    created: str
    modified: str
