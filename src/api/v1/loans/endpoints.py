from fastapi import FastAPI, APIRouter, status, Depends
from api.v1.loans.schemas import LoanSchema, LoanSchemaCreate
from api.v1.payments.schemas import PaymentSchema
from sqlalchemy.orm import Session
from db.session import get_session
from db.models import Loan, Payment
from fastapi.exceptions import HTTPException
from core.settings import settings
from core.utils.responses import EnvelopeResponse
from pydantic import BaseModel


router = APIRouter(prefix="/loans", tags=["Loans"])


# Método HTTP: POST
# Ruta: "/loan"
# Descripción: Crea un nuevo préstamo en la base de datos.
# Parámetros:
# loan: Objeto JSON que representa los datos del préstamo a crear. Debe seguir la estructura definida en LoanSchema.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve los datos del préstamo recién creado.
@router.post("/loan")
def create(loan: LoanSchemaCreate, db: Session = Depends(get_session)):
    new_loan = Loan(**loan.dict())
    fee = new_loan.amount * 0.15
    tax = fee * 0.16
    new_loan.fee = fee
    new_loan.tax = tax
    total = new_loan.amount + fee + tax
    db.add(new_loan)
    db.commit()
    db.refresh(new_loan)
    for item in 30:
        number = item
        quantity = total / 30
        payment_number = Payment({number=number})
        db.add(payment_number)
        db.commit()

    return new_loan


# Método HTTP: GET
# Ruta: "/loans"
# Descripción: Obtiene una lista de préstamos de la base de datos con paginación.
# Parámetros:
# db: Sesión de base de datos obtenida mediante la función get_session.
# limit: Número máximo de préstamos a devolver por página (por defecto 10).
# page: Página de resultados a devolver (por defecto 1).
# Respuesta: Devuelve una lista de préstamos según los parámetros proporcionados.
@router.get("/loans")
def get(db: Session = Depends(get_session), limit: int = 10, page: int = 1):
    skip = (page - 1) * limit
    all_loan = (((db.query(Loan).limit(limit)).offset(skip)).all())
    return all_loan


# Método HTTP: GET
# Ruta: "/loan/{id}"
# Descripción: Obtiene los detalles de un préstamo específico según su ID.
# Parámetros:
# id: ID del préstamo que se desea obtener.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve los datos del préstamo correspondiente al ID proporcionado.
@router.get("/loan/{id}")
def get_id(id: str, db: Session = Depends(get_session)):
    loan = db.query(Loan).filter(Loan.id == id).first()
    return loan


# Método HTTP: DELETE
# Ruta: "/loan/{id}"
# Descripción: Elimina un préstamo de la base de datos según su ID.
# Parámetros:
# id: ID del préstamo que se desea eliminar.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve un diccionario con datos del préstamo eliminado si existe, o devuelve un error 404 si el préstamo no existe.
@router.delete("/loan/{id}")
def delete(id: str, db: Session = Depends(get_session)):
    found = db.query(Loan).filter(Loan.id == id).first()
    if not found:
        raise HttpException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Doesn't exist")
    else:
        db.query(Loan).filter(Loan.id == id).delete()
        db.commit()
    return {"delete data": found}


# Método HTTP: PUT
# Ruta: "/loan/{id}"
# Descripción: Actualiza los datos de un préstamo existente en la base de datos.
# Parámetros:
# id: ID del préstamo que se desea actualizar.
# loan: Objeto JSON que contiene los datos actualizados del préstamo. Debe seguir la estructura definida en LoanSchema.
# db: Sesión de base de datos obtenida mediante la función get_session.
# Respuesta: Devuelve los datos actualizados del préstamo o un error 404 si el préstamo no existe.
@router.put("/loan/{id}")
def update(id: str, loan: LoanSchema, db: Session = Depends(get_session)):
    found = db.query(Loan).filter(Loan.id == id).first()
    if not found:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Loan with ID {id} does not exist")
    db.query(Loan).filter(Loan.id == id).update(loan.dict(), synchronize_session=False)
    db.commit()
    return loan.dict()