import unittest

from fastapi import status, Depends
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from db.session import get_session
from core.settings import settings
from db.models import Loan
from main import app


class TestLoanEndpoints(unittest.TestCase):
    def setUp(self):
        self.url = "/" + settings.API_V1 + "/loans"
        self.client = TestClient(app)

    def test_create_loan(self):
        # Prueba para la función create
        loan_data = {
            "created": "2024-03-22T23:16:28.856804+00:00",
            "amount": 1000.0,
            "id": "e5c26e4a-37b0-41be-b82e-89915480613e",
            "customer_id": "e85a4b19-f7be-4a18-5eb3-28fb503dbd46",
            "modified": "2024-03-22T23:16:28.856804+00:00"
        }
        response = self.client.post(self.url+"/loan", json=loan_data)
        self.assertEqual(response.status_code, 200)  # Verifica que se haya creado el préstamo
        created_loan = response.json()
        self.assertIn("id", created_loan)  # Verifica que el préstamo tenga un ID asignado

    def test_get_loans(self):
        # Prueba para la función get
        response = self.client.get(self.url+"/loans")
        self.assertEqual(response.status_code, 200)  # Verifica que se haya obtenido la lista de préstamos

    def test_get_loan_by_id(self, db: Session = Depends(get_session)):
        db_instance = db()

        # Prueba para la función get_id
        # Primero, creamos un préstamo para asegurarnos de que haya al menos uno en la base de datos
        new_loan = Loan(created="2024-03-22T23:16:28.856804+00:00", amount=1500.0,
                        id="e5c26e4a-37b0-41be-b82e-89915480613e", customer_id="e85a4b19-f7be-4a18-5eb3-28fb503dbd46",
                        modified="2024-03-22T23:16:28.856804+00:00")
        db.add(new_loan)
        db.commit()

        # Luego, intentamos obtener este préstamo por su ID
        response = self.client.get(self.url+f"/loan/{new_loan.id}")
        self.assertEqual(response.status_code, 200)  # Verifica que se haya obtenido el préstamo

    def test_delete_loan(self, db: Session = Depends(get_session)):
        # Prueba para la función delete
        # Primero, creamos un préstamo para luego eliminarlo
        new_loan = Loan(created="2024-03-22T23:16:28.856804+00:00", amount=1400.0,
                        id="e5c26e4a-37b0-41be-b82e-89915480613e", customer_id="e85a4b19-f7be-4a18-5eb3-28fb503dbd46",
                        modified="2024-03-22T23:16:28.856804+00:00")
        db.add(new_loan)
        db.commit()

        # Luego, intentamos eliminar este préstamo por su ID
        response = self.client.delete(self.url+f"/loan/{new_loan.id}")
        self.assertEqual(response.status_code, 200)  # Verifica que se haya eliminado el préstamo

    def test_update_loan(self, db: Session = Depends(get_session)):
        # Prueba para la función update
        # Primero, creamos un préstamo para luego actualizarlo
        new_loan = Loan(created="2024-03-22T23:16:28.856804+00:00", amount=5000.0,
                        id="e5c26e4a-37b0-41be-b82e-89915480613e", customer_id="e85a4b19-f7be-4a18-5eb3-28fb503dbd46",
                        modified="2024-03-22T23:16:28.856804+00:00")
        db.add(new_loan)
        db.commit()

        # Datos actualizados para el préstamo
        updated_data = {
            "created": "2024-03-22T23:16:28.856804+00:00",
            "amount": 1000.0,
            "id": "e5c26e4a-37b0-41be-b82e-89915480613e",
            "customer_id": "e85a4b19-f7be-4a18-5eb3-28fb503dbd46",
            "modified": "2024-03-22T23:16:28.856804+00:00"
        }

        # Actualizamos el préstamo con los datos actualizados
        response = self.client.put(self.url+f"/loan/{new_loan.id}", json=updated_data)
        self.assertEqual(response.status_code, 200)  # Verifica que se haya actualizado el préstamo
